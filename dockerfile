FROM ros:humble-ros-base-jammy
ARG DEBIAN_FRONTEND=noninteractive

# Set the working directory to /project
WORKDIR /project/

# Update and install basic tools
RUN apt-get update && apt-get upgrade -y
RUN apt-get update && apt-get install -y --no-install-recommends git nano wget
RUN apt-get update && apt-get install -y --no-install-recommends python3-pip
RUN pip3 install --upgrade pip
RUN pip3 install --no-cache-dir --upgrade setuptools

# Install jacolib
RUN git clone https://gitlab.com/Jaco-Assistant/jacolib.git
RUN pip3 install --no-cache-dir --upgrade -e jacolib

# Add ROS2 sourcing by default
RUN echo "source /opt/ros/humble/setup.bash" >> ~/.bashrc

# Create ROS2 workspace for project packages
RUN mkdir -p /project/dev_ws/src/
RUN /bin/bash -i -c 'cd /project/dev_ws/; colcon build'
RUN echo "source /project/dev_ws/install/setup.bash" >> ~/.bashrc

# Fix ros package building error
RUN pip3 install --no-cache-dir "setuptools<=58.2.0"

# Install project packages
COPY ./babbel_and_ihrs_msgs/ /project/BabbelAndIhrs/babbel_and_ihrs_msgs/
RUN ln -s /project/BabbelAndIhrs/babbel_and_ihrs_msgs/ /project/dev_ws/src/
RUN /bin/bash -i -c 'cd /project/dev_ws/; colcon build --symlink-install'
COPY ./babbel_and_ihrs/ /project/BabbelAndIhrs/babbel_and_ihrs/
RUN ln -s /project/BabbelAndIhrs/babbel_and_ihrs/ /project/dev_ws/src/
RUN /bin/bash -i -c 'cd /project/dev_ws/; colcon build --symlink-install'

# Update ros packages -> autocompletion and check packages
RUN /bin/bash -i -c 'ros2 pkg list'

# Clear cache to save space, only has an effect if image is squashed
RUN apt-get autoremove -y \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

CMD ["/bin/bash"]
