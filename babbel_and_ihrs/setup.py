from setuptools import setup

package_name = "babbel_and_ihrs"

setup(
    name=package_name,
    version="0.0.0",
    packages=[package_name],
    package_dir={package_name: "src/" + package_name},
    data_files=[
        ("share/ament_index/resource_index/packages", ["resources/" + package_name]),
        ("share/" + package_name, ["package.xml"]),
    ],
    install_requires=["setuptools"],
    zip_safe=True,
    author="user",
    author_email="user@todo.todo",
    maintainer="user",
    maintainer_email="user@todo.todo",
    keywords=["ROS", "ROS2"],
    classifiers=[
        "Intended Audience :: Developers",
        "License :: TODO",
        "Programming Language :: Python",
        "Topic :: Software Development",
    ],
    description="TODO: Package description.",
    tests_require=["pytest"],
    entry_points={
        "console_scripts": [
            "babbel = babbel_and_ihrs.action_babbel:main",
            "ihrs = babbel_and_ihrs.action_ihrs:main",
        ],
    },
)
