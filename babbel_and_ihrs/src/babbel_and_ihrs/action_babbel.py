import base64
import json
import os
import sys
import threading
import time

import rclpy
from jacolib import assistant, comm_tools
from rclpy.node import Node as rosNode

from babbel_and_ihrs_msgs.srv import SaySomething

# ==================================================================================================


def wav_to_base64(path: str) -> str:
    with open(path, "rb") as file:
        content = file.read()
    encoded: str = base64.b64encode(content).decode()
    return encoded


# ==================================================================================================

file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
assist: assistant.Assistant
node: rosNode

nlu_fin_wav = wav_to_base64(file_path + "skilldata/asr_finished.wav")
nlu_err_wav = wav_to_base64(file_path + "skilldata/asr_error.wav")
start_game_wav = wav_to_base64(file_path + "skilldata/start_game.wav")

asr_status_wav = {
    "SignalPositive": nlu_fin_wav,
    "SignalNegative": nlu_err_wav,
    "SignalStart": start_game_wav,
}

# ==================================================================================================


def callback_saytext(req, res):
    request = json.loads(req.jsontext)
    node.get_logger().info("Saying: " + str(request["text"]))
    res.answer = "{}"

    if request["text"] in list(asr_status_wav.keys()):
        # Play asr status sound
        play_status_sound(request["text"])

    elif "intent" not in request or request["intent"] == "":
        assist.publish_answer(request["text"], satellite="Default")

    else:
        answer = assist.publish_question(
            request["text"], question_intents=request["intent"], satellite="Default"
        )
        node.get_logger().info("Got answer: {}".format(answer))

        if answer != {}:
            msg = {}
            if answer["intent"]["name"] == "babbel_and_ihrs-confirm":
                msg = {"bool_answer": True}
            elif answer["intent"]["name"] == "babbel_and_ihrs-refuse":
                msg = {"bool_answer": False}
            elif answer["intent"]["name"] == "babbel_and_ihrs-set_name":
                solutions = assist.extract_entities(answer, "babbel_and_ihrs-name")
                if len(solutions) == 1:
                    name = solutions[0]
                    msg = {"set_name": name}

            if "bool_answer" in msg:
                play_status_sound("SignalPositive")
            elif "set_name" in msg:
                if msg["set_name"] == "":
                    play_status_sound("SignalNegative")
                else:
                    play_status_sound("SignalPositive")
            else:
                play_status_sound("SignalNegative")

            res.answer = json.dumps(msg)

    res.success = True
    node.get_logger().info("Successfully sent request to jaco: " + str(res))
    return res


# ==================================================================================================


def play_status_sound(status):
    pld = {
        "data": asr_status_wav[status],
        "timestamp": time.time(),
        "type": "input_status",
    }
    node.get_logger().info("Playing sound: " + status)
    output_topic = "Jaco/Default/AudioWav"
    msg_out = comm_tools.encrypt_msg(pld, output_topic)
    assist.mqtt_client.publish(output_topic, msg_out)
    assist.mqtt_client.loop_write()
    time.sleep(0.5)


# ==================================================================================================


def main():
    global assist, node

    rclpy.init(args=sys.argv)
    node = rclpy.create_node("babbel_node")

    _ = node.create_service(SaySomething, "ffrj_say_something", callback_saytext)
    node.get_logger().info("Started service for babbel and ihrs")

    assist = assistant.Assistant(repo_path=file_path)

    threading.Thread(target=assist.run).start()
    rclpy.spin(node)


# ==================================================================================================

if __name__ == "__main__":
    main()
