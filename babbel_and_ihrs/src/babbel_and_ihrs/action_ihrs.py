import json
import os
import sys
import threading

import rclpy
from jacolib import assistant
from rclpy.executors import MultiThreadedExecutor
from rclpy.node import Node as rosNode
from rclpy.publisher import Publisher as rosPublisher
from rclpy.qos import QoSHistoryPolicy, QoSProfile, QoSReliabilityPolicy
from std_msgs.msg import String

# ==================================================================================================

file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
assist: assistant.Assistant
node: rosNode
publisher: rosPublisher


# ==================================================================================================


def publish_and_log(msg):
    """Publish to hear callback topic"""

    jsontext = json.dumps(msg)
    node.get_logger().info("Publishing hear callback: " + jsontext)

    msg = String()
    msg.data = jsontext
    publisher.publish(msg)


# ==================================================================================================


def callback_get_tipp(message):
    # Send finish signal here as backup if it did get lost somewhere
    msg = {"currently_speaking": False}
    publish_and_log(msg)

    msg = {"get_tipp": True}
    publish_and_log(msg)


# ==================================================================================================


def callback_confirm(message):
    msg = {"bool_answer": True}
    publish_and_log(msg)


# ==================================================================================================


def callback_refuse(message):
    msg = {"bool_answer": False}
    publish_and_log(msg)


# ==================================================================================================


def callback_set_name(message):
    msg = {"set_name": True}
    publish_and_log(msg)


# ==================================================================================================


def callback_get_deck_count(message):
    msg = {"get_deck_count": True}
    publish_and_log(msg)


# ==================================================================================================


def callback_finished_speaking(message):
    if message["playing"] is False:
        msg = {"currently_speaking": False}
        publish_and_log(msg)


# ==================================================================================================


def run_executor(executor, n):
    executor.add_node(n)
    try:
        executor.spin()
    finally:
        executor.shutdown()


# ==================================================================================================


def main():
    global assist, node, publisher

    rclpy.init(args=sys.argv)
    node = rclpy.create_node("ihrs_node")

    assist = assistant.Assistant(repo_path=file_path)

    # Add Jaco topic callbacks
    assist.add_topic_callback("get_deck_count", callback_get_deck_count)
    assist.add_topic_callback("get_tipp", callback_get_tipp)
    assist.add_topic_callback("set_name", callback_set_name)
    assist.add_topic_callback("confirm", callback_confirm)
    assist.add_topic_callback("refuse", callback_refuse)
    assist.add_topic_callback("Jaco/StreamWav/Status", callback_finished_speaking)

    # Quality of service settings
    qos_profile = QoSProfile(
        reliability=QoSReliabilityPolicy.RELIABLE,
        history=QoSHistoryPolicy.KEEP_LAST,
        depth=1,
    )

    # Start ROS node
    publisher = node.create_publisher(String, "/ffrj_hearback_topic", qos_profile)

    # Run executor that uses a thread pool in another thread so main thread is not blocked
    exec = MultiThreadedExecutor(num_threads=None)
    thread = threading.Thread(target=run_executor, args=(exec, node), daemon=True)
    thread.start()

    assist.run()


# ==================================================================================================

if __name__ == "__main__":
    main()
