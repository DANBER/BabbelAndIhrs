## lookup:name
name.txt

## intent:refuse
- wieso sollte ich
- kein bock
- keine lust
- ne
- nein
- nein danke
- ne ich mag nicht mehr
- nicht
- nein nein
- nein, auf keinen fall
- niemals
- nein, leider falsch
- das ist nicht richtig
- hast du falsch erkannt
- das war nicht richtig
- das ist nicht mein name
- das war nicht korrekt
 
## intent:confirm
- passt
- super
- machen wir
- ok
- natuerlich
- gerne
- ja
- ja ja
- sehr gerne
- ja gerne
- jawohl
- supidupi
- klar
- supi dupi
- ja, das stimmt
- das ist richtig
- hast du richtig erkannt
- das ist mein name
- jawohl sehr gerne
- das ist korrekt
- das war richtig

## intent:get_tipp
- Sag mir was ich machen soll
- Gib bitte einen Hinweis
- Ich brauche ein Tipp
- Was soll ich tun?
- Gib mir einen Tipp
- Tipp
- Einen Tipp bitte
- Ich möchte einen Tipp

## intent:get_deck_count
- Sag mir bitte den Deckwert
- Wie ist der Kartenstand?
- Wie ist der Deckwert?
- Wie ist hoch ist der Deckwert?

## intent:set_name
- Du kannst mich [Wolfgang](name) nennen
- [Sophie](name)
- Ich bin [Markus](name)
- Mein Name heißt [Lisa](name)
- Nenne mich [Thomas](name)
- Mein Name ist [Alex](name)
- Ich heiße [Daniel](name)
