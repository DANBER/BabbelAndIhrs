# Babbel and Ihrs

Demo ROS2 interface for Jaco, originally from the _RoboJack_ project.

Please note that this skill doesn't work standalone and requires some manual installation steps.

<br>

The interesting files can be found in: [babbel_and_ihrs/src/babbel_and_ihrs/](babbel_and_ihrs/src/babbel_and_ihrs/)

<br>

Build docker container: 

```bash
docker build -f dockerfile -t babbel_and_ihrs .
```

Training with Jaco:
- Copy babbel_and_ihrs/scr/babbel_and_ihrs to Jaco's skill directory
- Train stt+nlu models
- Generate skill topic keys and copy them back to babbel_and_ihrs/scr/babbel_and_ihrs/skilldata

Run and test:
```bash
# 1. Start Jaco-Master and Jaco-Satellite

# 2. Start interface containers
# Depending on the used device you might need to update this file a bit
docker-compose -f docker-compose.yml up

# 3. Connect to testing container
docker exec -it babbelandihrs_test_node_1 bash

# 4. Call ros services by hand:
ros2 service call /ffrj_say_something babbel_and_ihrs_msgs/SaySomething "{jsontext: '{\"text\":\"Hallo K\\u00e4se\"}'}"
ros2 service call /ffrj_say_something babbel_and_ihrs_msgs/SaySomething "{jsontext: '{\"text\":\"Willst du eine Runde Karten mit mir spielen?\", \"intent\":[\"babbel_and_ihrs-confirm\", \"babbel_and_ihrs-refuse\"]}'}"
ros2 service call /ffrj_say_something babbel_and_ihrs_msgs/SaySomething "{jsontext: '{\"text\":\"Wie heisst du?\", \"intent\":[\"babbel_and_ihrs-set_name\"]}'}"
```
